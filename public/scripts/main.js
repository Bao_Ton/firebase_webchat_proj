'use strict';

var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var chatSection = document.getElementById('chat-box');

var messageList = document.getElementById('messages');
var submitButton = document.getElementById('submit');
var messageForm = document.getElementById('message-send-form');
var messageInput = document.getElementById('message-to-send');

var messagesRef;

window.addEventListener('load', function() {

  signInButton.addEventListener('click', function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
  });

  signOutButton.addEventListener('click', function() {
    firebase.auth().signOut();
  });

  firebase.auth().onAuthStateChanged(onAuthStateChanged);

  showSection(chatSection);

  messageForm.addEventListener('submit', addMessage);

  messageInput.addEventListener('keyup', toggleButton);
  messageInput.addEventListener('change', toggleButton);

}, false);

// Keep track of current user ID for auth event changes.
var currentUID;

function onAuthStateChanged(user) {
  if(user && currentUID === user.uid) {
    return;
  }

  if(user) {
    currentUID = user.uid;
    splashPage.style.display = 'none';
    loadMessages();
    var usernameBar = document.getElementById('username-bar');
    usernameBar.innerHTML = firebase.auth().currentUser.displayName;
  }
  else {
    currentUID = null;
    splashPage.style.display = '';
  }
}

function showSection(sectionElement) {
  if(sectionElement) {
    sectionElement.style.display = 'block';
  }
}

function loadMessages() {

  // This should be reset in case a refresh.
  messageList.innerHTML = '';

  messagesRef = firebase.database().ref('messages');
  messagesRef.off();

  var setMessage = function(data) {
    var val = data.val();
    displayMessage(data.key, val.name, val.text);
  }

  messagesRef.limitToLast(10).on('child_added', setMessage);
  messagesRef.limitToLast(10).on('child_changed', setMessage);
}

var MESSAGE =
  '<div class="message-container talk-bubble triangle">' +
    '<div class="name nametext"></div>' +
    '<div class="message talktext"></div>' +
  '</div>';

function displayMessage(key, name, text) {
  var div = document.getElementById(key);
  if(!div) {
    var container = document.createElement('div');
    container.innerHTML = MESSAGE;
    div = container.firstChild;
    div.setAttribute('id', key);
    messageList.appendChild(div);
  }

  var currentUser = firebase.auth().currentUser;
  div.querySelector('.name').textContent = "From: " + name;
  if(name === currentUser.displayName) {
    div.style["margin-left"] = "60%";
    div.style["box-shadow"] = "-10px 5px 5px #888888";
    div.className += ' right';
  }
  else {
    div.className += ' left';
  }

  var messageElement = div.querySelector('.message');
  if(text) {
    messageElement.textContent = text;
  }

  messageList.scrollTop = messageList.scrollHeight;
}

function addMessage(e) {
  e.preventDefault();
  if(messageInput.value && checkSignedIn()) {
    var currentUser = firebase.auth().currentUser;
    messagesRef.push({
      name: currentUser.displayName,
      text: messageInput.value
    }).then(function() {
      messageInput.value = '';
      toggleButton();
      messageList.scrollTop = messageList.scrollHeight;
    }).catch(function(error) {
      console.error('Error writing new message', error);
    });
  }
}

function checkSignedIn() {
  if(firebase.auth().currentUser) {
    return true;
  }
  return false;
}

function toggleButton() {
  if(messageInput.value) {
    submitButton.removeAttribute('disabled');
  }
  else {
    submitButton.setAttribute('disabled', 'true');
  }
}

// TODO: Add presence system
