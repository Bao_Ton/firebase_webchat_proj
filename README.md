This is a basic real-time webchat application using firebase real-time database.

The chat application allows for any number of users to enter and begin chatting using google authorization.
Users must login before they are allowed to chat.

Other users chat messages are displayed on the left while chat messages of the currently logged in user will be displayed on the right.
A maximum of the last 10 messages will be displayed on login.

There is currently no presence system to detect the number of online users. (TODO)
There is no support for mobile users. (TODO)

The application is being hosted at http://fir-webchat-beef7.firebaseapp.com/
